import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mobile_project/data/data.dart';
import 'package:mobile_project/models/models.dart';
import 'screens.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final CollectionReference _collectionRef =
      FirebaseFirestore.instance.collection('users');

  Future<void> getData() async {
    QuerySnapshot querySnapshot =
        await _collectionRef.orderBy('score', descending: true).get();
    final List<User> allData = querySnapshot.docs.map((doc) {
      return User(
        name: doc['name'],
        pass: doc['password'],
        imageUrl: doc['imageUrl'],
        score: doc['score'],
      );
    }).toList();
    allUser = allData;
  }

  String? user;
  String? password;
  @override
  void initState() {
    super.initState();
    getData();
    user = '';
    password = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 500.0,
          width: 400.0,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(30.0),
            ),
            color: Colors.white,
          ),
          padding: const EdgeInsets.all(30.0),
          child: ListView(
            children: [
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    const Text(
                      'Login',
                      style: TextStyle(color: Colors.black54, fontSize: 50.0),
                    ),
                    const SizedBox(height: 60.0),
                    TextFormField(
                      initialValue: user,
                      decoration: const InputDecoration(labelText: 'Username'),
                      onChanged: (String? value) {
                        user = value!;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please input Username';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 20.0),
                    TextFormField(
                      initialValue: password,
                      decoration: const InputDecoration(labelText: 'Password'),
                      onChanged: (String? value) {
                        password = value!;
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please input Password';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 80.0),
                    ElevatedButton(
                      onPressed: () {
                        bool _checkUser = false;
                        bool _checkPass = false;
                        for (int i = 0; i < allUser.length; i++) {
                          if (user == allUser[i].name &&
                              password == allUser[i].pass) {
                            _checkUser = true;
                            _checkPass = true;
                            setState(() {
                              currentUser = i;
                            });
                          }
                        }
                        if (!_checkUser && !_checkPass) {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              title: const Text('Login Fail!'),
                              content: const Text('Please, Try again.'),
                              actions: [
                                TextButton(
                                  onPressed: () =>
                                      Navigator.pop(context, 'Continue'),
                                  child: const Text('Continue'),
                                )
                              ],
                            ),
                          );
                        } else {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (builder) => const NavScreen()),
                          );
                        }
                      },
                      child: const Text('Login'),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.grey.shade800,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 50, vertical: 20),
                        textStyle: const TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                        side: const BorderSide(
                            width: 3.0, color: Colors.amberAccent),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
