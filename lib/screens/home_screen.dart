import 'package:flutter/material.dart';
import 'package:mobile_project/models/models.dart';
import 'package:mobile_project/screens/screens.dart';
import 'package:mobile_project/widgets/widgets.dart';

class HomeScreen extends StatefulWidget {
  final User user;

  const HomeScreen({Key? key, required this.user}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.all(30.0),
        margin: const EdgeInsets.all(30.0),
        width: 500.0,
        decoration: const BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.all(Radius.circular(12)),
          boxShadow: [BoxShadow(color: Colors.amberAccent, spreadRadius: 4.0)],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              children: [
                ProfileAvatar(
                  imageUrl: widget.user.imageUrl,
                ),
                const SizedBox(height: 10.0),
                Text(
                  widget.user.name,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 35.0,
                  ),
                ),
                const SizedBox(height: 50.0),
                const Text(
                  'BEST SCORE',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 35.0,
                  ),
                ),
                const SizedBox(height: 10.0),
                Text(
                  '${widget.user.score}',
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 35.0,
                  ),
                ),
              ],
            ),
            const Spacer(),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(
                  context,
                  MaterialPageRoute(builder: (builder) => const LoginScreen()),
                );
              },
              child: const Text('LogOut'),
              style: ElevatedButton.styleFrom(
                primary: Colors.grey.shade800,
                padding:
                    const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                textStyle: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
                side: const BorderSide(width: 3.0, color: Colors.amberAccent),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
