import 'package:flutter/material.dart';
import 'package:mobile_project/data/data.dart';
import 'package:mobile_project/models/models.dart';

class PlayScreen extends StatefulWidget {
  final List<ButtonTab> tab;

  const PlayScreen({
    Key? key,
    required this.tab,
  }) : super(key: key);

  @override
  State<PlayScreen> createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  int _num = 0;
  @override
  void initState() {
    _num = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void _update(int n) {
      setState(() => _num += n);
    }

    return Container(
      padding: const EdgeInsets.all(30.0),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 350.0,
              height: 100.0,
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.black38,
              ),
              child: Center(
                // ignore: prefer_const_literals_to_create_immutables
                child: Row(children: [
                  const Text(
                    'You Score :',
                    style: TextStyle(color: Colors.white70, fontSize: 30.0),
                  ),
                  const Spacer(),
                  Text(
                    '$_num',
                    style:
                        const TextStyle(color: Colors.white70, fontSize: 30.0),
                  ),
                ]),
              ),
            ),
            const SizedBox(height: 50.0),
            SizedBox(
              width: 350.0,
              height: 350.0,
              child: CreateButton(
                tab: tab,
                update: _update,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CreateButton extends StatelessWidget {
  final List<ButtonTab> tab;
  final ValueChanged<int> update;

  const CreateButton({
    Key? key,
    required this.tab,
    required this.update,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: tab.length,
      gridDelegate:
          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
      itemBuilder: (BuildContext context, int index) {
        return ElevatedButton.icon(
          onPressed: () {
            update(1);
          },
          icon: Icon(tab[index].buttonIcon, size: 75.0),
          label: const Text(''),
          style: ElevatedButton.styleFrom(primary: tab[index].buttonColor),
        );
      },
    );
  }
}
