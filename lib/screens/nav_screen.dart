import 'dart:math';
import 'package:flutter/material.dart';
import 'package:mobile_project/data/data.dart';
import 'package:mobile_project/models/models.dart';
import 'package:mobile_project/screens/screens.dart';
import 'package:mobile_project/widgets/widgets.dart';

class NavScreen extends StatefulWidget {
  const NavScreen({Key? key}) : super(key: key);
  @override
  State<NavScreen> createState() => _NavScreenState();
}

class _NavScreenState extends State<NavScreen> {
  @override
  void initState() {
    super.initState();
    for (var i = 0; i < 9; i++) {
      tab[i] =
          ButtonTab(i + 1, Colors.amberAccent, gg[Random().nextInt(gg.length)]);
    }
  }

  final List<Widget> _screens = [
    HomeScreen(user: allUser[currentUser]),
    PlayScreen(tab: tab),
    ScoreboardScreen(allUser: allUser),
  ];

  final List<IconData> _icons = const [
    Icons.home,
    Icons.play_arrow_rounded,
    Icons.analytics_outlined,
  ];
  int _selecedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return DefaultTabController(
      length: _icons.length,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(screenSize.width, 100),
          child: CustomTabBar(
            icons: _icons,
            selecedIndex: _selecedIndex,
            onTap: (index) => setState(() => _selecedIndex = index),
          ),
        ),
        body: IndexedStack(
          index: _selecedIndex,
          children: _screens,
        ),
      ),
    );
  }
}
