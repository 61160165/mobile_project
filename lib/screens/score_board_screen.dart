import 'package:flutter/material.dart';
import 'package:mobile_project/data/data.dart';
import 'package:mobile_project/models/models.dart';
import 'package:mobile_project/widgets/widgets.dart';

class ScoreboardScreen extends StatelessWidget {
  final List<User> allUser;
  const ScoreboardScreen({
    Key? key,
    required this.allUser,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: const EdgeInsets.all(30.0),
        margin: const EdgeInsets.all(30.0),
        width: 400.0,
        decoration: const BoxDecoration(
          color: Color(0xff616161),
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [BoxShadow(color: Colors.amberAccent, spreadRadius: 5.0)],
        ),
        child: Expanded(
          child: ListView(
            children: [
              const Center(
                child: Text(
                  'Score Board',
                  style: TextStyle(color: Colors.amberAccent, fontSize: 30.0),
                ),
              ),
              const Divider(
                height: 20,
                thickness: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    'No.',
                    style: TextStyle(color: Colors.amberAccent, fontSize: 15.0),
                  ),
                  Text(
                    'Username.',
                    style: TextStyle(color: Colors.amberAccent, fontSize: 15.0),
                  ),
                  Text(
                    'Best Score.',
                    style: TextStyle(color: Colors.amberAccent, fontSize: 15.0),
                  ),
                ],
              ),
              const Divider(
                height: 20,
                thickness: 5,
              ),
              Expanded(
                child: Column(
                  children: List.generate(
                    allUser.length,
                    (index) => userList(allUser[index], index + 1),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Row userList(user, i) {
  return Row(
    children: [
      Text('$i.',
          style: const TextStyle(color: Colors.amberAccent, fontSize: 15.0)),
      const Spacer(flex: 1),
      SizedBox(
        height: 50.0,
        child: Row(
          children: [
            ProfileAvatar(imageUrl: user.imageUrl),
            Text(
              CheckUser(user.name) == true ? '( You )' : user.name,
              style: const TextStyle(
                color: Colors.amberAccent,
                fontSize: 15.0,
              ),
            ),
          ],
        ),
      ),
      const Spacer(flex: 1),
      Text('${user.score}',
          style: const TextStyle(color: Colors.amberAccent, fontSize: 15.0)),
    ],
  );
}

bool CheckUser(String u) {
  if (allUser[currentUser].name == u) {
    return true;
  } else {
    return false;
  }
}
