import 'package:flutter/material.dart';

class ButtonTab {
  final int id;
  Color buttonColor;
  IconData buttonIcon;

  ButtonTab(
    this.id,
    this.buttonColor,
    this.buttonIcon,
  );
}
