class User {
  final String name;
  final String pass;
  final String imageUrl;
  final int score;

  User({
    required this.name,
    required this.pass,
    required this.imageUrl,
    required this.score,
  });
}
